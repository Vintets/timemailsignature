#!/usr/bin/env python

"""
Проект TimeMailSignature
Создание html шаблонов подписи для корпоративного e-mail
/*******************************************************
 * Written by Vintets <programmer@vintets.ru>, March 2018
 * This file is part of TimeMailSignature project.
*******************************************************/

# for python 3.9.7 and over
"""

import os.path
import sys
import time
import json  # noqa F401
import xlrd
from accessory import authorship, clear_console, cprint, check_version, exit_from_program


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
__version_info__ = ('3', '0', '0')
__version__ = '.'.join(__version_info__)
__author__ = 'master by Vint'
__title__ = '--- Time_Mail_Signature ---'
__copyright__ = 'Copyright 2018 (c)  bitbucket.org/Vintets'
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class SettingsMSXLS():
    path_data       = 'in_out'
    path_data_out   = os.path.join('in_out', 'Signature')
    filename_xls    = 'Person_data.xls'
    p_              = 'TimeImage/'
    p_express       = 'C:\\Program Files\\Common Files\\Microsoft Shared\\Stationery\\TimeImage/'
    p_logo          = 'https://static.insales-cdn.com/files/1/5061/16200645/original/time-logo.png'
    icon_vk         = 'https://static.insales-cdn.com/files/1/6784/30087808/original/logo_40_vk.png'
    icon_ok         = 'https://static.insales-cdn.com/files/1/6787/30087811/original/logo_40_ok.png'
    icon_yt         = 'https://static.insales-cdn.com/files/1/6789/30087813/original/logo_40_youtube.png'
    icon_zn         = 'https://static.insales-cdn.com/files/1/6792/30087816/original/logo_40_zen.png'
    # Old icons
    # icon_vk       = 'https://static.insales-cdn.com/files/1/4016/23834544/original/40_vk.png'
    # icon_fb       = 'https://static.insales-cdn.com/files/1/4020/23834548/original/40_facebook.png'
    # icon_yt       = 'https://static.insales-cdn.com/files/1/4043/23834571/original/40_youtube.png'
    # icon_inst     = 'https://static.insales-cdn.com/files/1/4023/23834551/original/40_instagram_logo_2016.png'
    banner1         = 'https://static.insales-cdn.com/files/1/4438/23834966/original/banner1.gif'
    banner2         = 'https://static.insales-cdn.com/files/1/4461/23834989/original/banner2.gif'

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    col_not_person  = 5  # 2 пустая колонка по которой определяем наличие данных
    col_assistant   = 11  # колонка с именем помощника

    coltab_value = {
                    1: ('номер', 'int'),
                    2: ('ФИО', 'str'),
                    3: ('программа', 'str'),
                    4: ('должность', 'str'),
                    5: ('телефон', 'int'),
                    6: ('мобильный', 'str'),
                    7: ('mail', 'str'),
                    8: ('ICQ', 'str'),
                    9: ('skype', 'str'),
                    10: ('nop', 'str'),
                    11: ('помощник ФИО', 'str'),
                    12: ('помощник телефон', 'int'),
                    13: ('помощник мобильный', 'str'),
                    14: ('помощник mail', 'str'),
                    15: ('помощник ICQ', 'str'),
                    16: ('комментарии', 'str'),
                    }

    ht_start = '''<!DOCTYPE html PUBLIC "-//W3C//DTD>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
</head>
<body>

<div><br /><br /><br /></div>
'''

    ht1 = '''<div style='margin-left:20.0pt;margin-top:20.0pt;font-size:11.0pt;font-family:"Calibri","sans-serif";color:#1f497d;'>
С уважением,
</div>

<div style='margin-left:18.0pt;font-size:11.0pt;font-family:"Calibri","sans-serif";color:#1f497d;'>
<table border="0">
<tbody>
<tr>
<td valign="top" style='color:#1f497d;'>
'''
    ht4 = '<br />\n'
    ht9 = '</td>\n'
    ht10 = '''<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign="top" style="color:#1f497d;">также можете обращаться к:<br />
'''
    ht16 = '''</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td valign="top" style='color:#1f497d;'>&nbsp;</td>
'''
    ht17 = '</tr>\n</tbody>\n</table>\n</div>\n\n'
    # ссылочная часть
    htf18 = '''<div style='margin-left:20.0pt;margin-top:5.0pt;font-size:11.0pt;font-family:"Calibri","sans-serif";color:#1f497d;'>
<a href="https://teplonet.ru" target="_blank"><img alt="Перейти на наш сайт" src="'''
    htf19 = '''" border="0"></a><br />
Офис и склад: Москва, ул. Суздальская, 46<br />
<a href="https://teplonet.ru" target="_blank">www.teplonet.ru</a>
</div>

<div style='margin-left:20.0pt;margin-top:5.0pt;font-size:11.0pt;font-family:"Calibri","sans-serif";color:#1f497d;'>
<strong>Наши группы в соцсетях:</strong>
</div>

<div style='margin-left:20.0pt;margin-top:3.0pt;'>
'''
    htf20 = f'''<a href="https://vk.com/time_teplonet" target="_blanc"><img alt="ВКонтакте" src="{icon_vk}" border="0" style="width: 40px; height: 40px;" /></a>&nbsp;&nbsp;&nbsp;'''
    htf21 = f'''<a href="https://ok.ru/group/55096271765612" target="_blanc"><img alt="Одноклассники" src="{icon_ok}" border="0" style="width: 40px; height: 40px;" /></a>&nbsp;&nbsp;&nbsp;'''
    htf22 = f'''<a href="https://www.youtube.com/channel/UCeESTtAUn6iD7T_vbyU8Y3Q" target="_blanc"><img alt="YouTube" src="{icon_yt}" border="0" style="width: 40px; height: 40px;" /></a>&nbsp;&nbsp;&nbsp;'''
    htf23 = f'''<a href="https://dzen.ru/id/625c65b4ab68c7653f8951b5" target="_blanc"><img alt="Яндекс Дзен" src="{icon_zn}" border="0" style="width: 40px; height: 40px;" /></a>'''
    htf24_1 = '''
</div>
'''
    htf24_2 = '''
<div style='margin-left:20.0pt;margin-top:5.0pt;font-size:11.0pt;font-family:"Calibri","sans-serif";color:#1f497d;'>
подписывайтесь, будет много интересного
</div>
'''
    htf24_3 = '''
<div style='margin-left:20.0pt;margin-top:5.0pt;'>
<hr>
</div>

<div style='margin-left:20.0pt;'>
'''
    htf24 = htf24_1 + htf24_3
    htf25 = f'''<a href="https://teplonet.ru/banner1.html" target="_blank"><img src="{banner1}" border="0"></a>'''
    htf26 = f'''<a href="https://teplonet.ru/banner2.html" target="_blank"><img src="{banner2}" border="0"></a>'''
    htf27 = '''
</div>'''

    ht_final = '</body>\n</html>'

    ht_link_ym = [
                htf18, p_logo, htf19,
                htf20, htf21,
                htf22, htf23,
                htf24,
                htf25, htf26,
                htf27]
    """
    ht_link    = [
                htf18, p_, htf19,
                p_, htf20, p_, htf21, p_, htf22, p_,
                htf23, p_, htf24, p_, htf25, p_, htf26, p_,
                htf27]
    ht_link_ex = [
                htf18, p_express, htf19,
                p_express, htf20, p_express, htf21, p_express, htf22, p_express,
                htf23, p_express, htf24, p_express, htf25, p_express, htf26, p_express,
                htf27]
    """


class MyErrors():
    filelog = os.path.join(SettingsMSXLS.path_data, 'log_Time_Mail_Signature.txt')

    def errors(self, _type, arg='', stop=True):
        if _type == 'noOpen':
            err = f'13Ошибка открытия файла: {arg}'
            errlg = f'13Ошибка открытия файла: ^15_{arg}'
        elif _type == 'load':
            err = errlg = '13Не получили файл'
        elif _type == 'filename':
            err = errlg = '13Не получили имя файла'
        elif _type == 'blocket':
            err = f'13Файл {arg} заблокирован для записи'
            errlg = f'13Файл ^15_{arg} ^13_заблокирован для записи'
        elif _type == 'abort':
            err = errlg = '13Отменено создание и перезапись шаблонов'
        self.add_log(err[2:])
        cprint(errlg)
        cprint('14---------------   ^12_ERROR   ^14_---------------')
        if stop:
            input()
            exit(1)

    def add_log(self, text):
        current_time = time.strftime('%Y-%m-%d  %H:%M:%S    ', time.localtime(time.time()))
        with open(MyErrors.filelog, 'a', encoding='utf-8') as f:
            f.write(f'{current_time}{text}\n')


class ImportXLS(MyErrors):
    def __init__(self):
        self.path_data      = SettingsMSXLS.path_data
        self.XLS            = os.path.join(self.path_data, SettingsMSXLS.filename_xls)
        self.person_data    = []
        self.rb             = None
        self.rs             = None

    def parse_xls(self):
        self.check_read_file(self.XLS)
        self.reading_xls()
        self.statistics_xls()
        self.parser()
        cprint(f'2Файл XLS импортирован. Пользователей: ^15_{len(self.get_person_data())}')

    def check_read_file(self, fullfilename):
        if not os.path.isfile(fullfilename):
            self.errors('noOpen', fullfilename)

    def reading_xls(self):
        self.rb = xlrd.open_workbook(self.XLS, formatting_info=True)
        # self.rb.sheet_loaded(0)  # Загружаем лист
        # self.rs = self.rb.get_sheet(0)  # Читаем из первого листа если все листы загружены (on_demand=True)
        self.rs = self.rb.sheet_by_index(0)

    def statistics_xls(self):
        cprint(f'1В файле XLS строк: ^15_{self.rs.nrows} (-2)  ^1_колонок: ^15_{self.rs.ncols} \n')
        # Первая строка - заголовок
        # print(json.dumps(self.rs.row_values(0), ensure_ascii=False))
        # Последняя
        # print(json.dumps(self.rs.row_values(self.rs.nrows-1), ensure_ascii=False))
        pass

    def parser(self):
        self.person_data = []
        # self.cur_subdir = SettingsMSXLS.sub_dirs_dict['default']
        # for row in range(3, 4):
        # for row in range(self.rs.nrows - 3, self.rs.nrows):
        for row in range(2, self.rs.nrows):
            # print('\nстрока', row)
            person = {}
            data = self.rs.row_values(row)
            # print(json.dumps(data, ensure_ascii=False))
            if not self.is_data(data):
                continue
            if len(SettingsMSXLS.coltab_value) != len(data):
                self.errors('coltab')
            for n, p in enumerate(data):
                p = self.correct_type(p, SettingsMSXLS.coltab_value[n + 1][1])
                person[SettingsMSXLS.coltab_value[n + 1][0]] = p

            # c = self.rs.cell(row, 0)
            # xf = self.rs.book.xf_list[c.xf_index]
            # fmt_obj = self.rs.book.format_map[xf.format_key]
            # print(row, repr(c.value), c.ctype, fmt_obj.type, fmt_obj.format_key, fmt_obj.format_str)
            # print(json.dumps(person, indent=4, ensure_ascii=False))
            self.person_data.append(person)
        # print(json.dumps(self.person_data, indent=2, ensure_ascii=False))
        pass

    def get_person_data(self):
        return self.person_data

    def is_data(self, data):
        if data[SettingsMSXLS.col_not_person - 1] != '':
            return True
        return False

    def correct_type(self, p, typs):
        if p and typs == 'int':
            p = int(p)
        return p


class Signature(MyErrors):
    def __init__(self, person_data):
        self.person_data    = person_data

    def run(self):
        # print(json.dumps(self.person_data, indent=2, ensure_ascii=False))
        for person in self.person_data:
            filename    = self.get_filename(person['mail'], pref='_')  # noqa F841
            filename_ex = self.get_filename(person['mail'], pref='_ex')  # noqa F841
            filename_ym = self.get_filename(person['mail'], pref='_YM')
            ht_start    = SettingsMSXLS.ht_start  # noqa F841
            ht1         = SettingsMSXLS.ht1
            ht4         = SettingsMSXLS.ht4
            ht9         = SettingsMSXLS.ht9
            ht10        = SettingsMSXLS.ht10
            ht16        = SettingsMSXLS.ht16
            ht16        = SettingsMSXLS.ht16
            ht17        = SettingsMSXLS.ht17
            # ht_link     = SettingsMSXLS.ht_link
            # ht_link_ex  = SettingsMSXLS.ht_link_ex
            ht_link_ym  = SettingsMSXLS.ht_link_ym
            ht_final    = SettingsMSXLS.ht_final  # noqa F841

            # ФИО
            ht2_name = f'<strong>{person["ФИО"]}</strong><br />\n'
            # должность
            ht3_position = self.set_position(person['должность'])
            # телефон
            ht5_tel = self.set_tel(person['телефон'])
            # мобильный
            ht6_mob = self.set_mob(person['мобильный'])
            # mail эл-почта
            ht7_mail = self.set_mail(person['mail'])
            # icq, skype
            ht8_icq = self.set_icq(person['ICQ'], person['skype'])

            html_list = [ht1, ht2_name, ht3_position, ht4, ht5_tel,
                         ht6_mob, ht7_mail, ht8_icq, ht9]

            if person['помощник ФИО']:
                # ФИО помощник
                ht11_name = f'<strong>{person["помощник ФИО"]}</strong><br /><br />\n'
                # телефон помощник
                ht12_tel = self.set_tel(person['помощник телефон'])
                # мобильный помощник
                ht13_mob = self.set_mob(person['помощник мобильный'])
                # mail эл-почта помощник
                ht14_mail = self.set_mail(person['помощник mail'])
                # icq, skype помощник
                ht15_icq = self.set_icq(person['помощник ICQ'], '')
                html_list.extend([ht10, ht11_name, ht12_tel, ht13_mob,
                                  ht14_mail, ht15_icq, ht16])

            html_list.append(ht17)

            # html_full = [ht_start] + html_list + ht_link + [ht_final]
            # html_express = [ht_start] + html_list + ht_link_ex + [ht_final]
            html_YM = html_list + ht_link_ym  # noqa N806

            # self.save_file(filename, ''.join(html_full))
            # self.save_file(filename_ex, ''.join(html_express))
            self.save_file(filename_ym, ''.join(html_YM))

            # print(''.join(html_full))
            # print('filename', filename)
            # break
        self.log_ok()

    def set_position(self, position):
        if position:
            return f'{position}<br />\n'
        return ''

    def set_tel(self, tel):
        if tel:
            return f'тел. +7 (495) 258-93-88 (доб. {tel})<br />\n'
        return ''

    def set_mob(self, mob):
        if mob:
            return f'моб. +{mob}<br />\n'
        return ''

    def set_mail(self, mail):
        if mail:
            return f'e-mail: <a href="mailto:{mail}" style="color:#15d;">{mail}</a><br />\n'
        return ''

    def set_icq(self, icq, skype):
        text = ''
        if icq or skype:
            if icq:
                text = f'ICQ {icq}'
                if skype:
                    text += ', '
            if skype:
                text += f'SKYPE {skype}'
        return text

    def get_filename(self, mail, pref=''):
        name_mail = mail.split('@')
        domen = name_mail[1].split('.')[0]
        return f'_Подпись_Тайм{pref}_{name_mail[0]}-{domen}.html'

    def save_file(self, filename, text):
        file = os.path.join(SettingsMSXLS.path_data_out, filename)
        with open(file, 'w', encoding='cp1251') as f:
            f.write(text)

    def log_ok(self):
        cprint('2Обработка окончена')
        self.add_log('Обработка окончена\n')


def warning_all():
    cprint('12Внимание! ^14_Запустить создание и перезапись шаблонов? Подтвердить (Y): ', end='')
    response = input().lower()
    response = response.lower()
    if response != 'y':
        MyErrors().errors('abort')


def main():
    warning_all()
    cprint('1Запуск...')

    dataXLS = ImportXLS()  # noqa N806
    dataXLS.parse_xls()
    Signature(dataXLS.get_person_data()).run()


if __name__ == '__main__':
    _width = 130
    _hight = 54
    if sys.platform == 'win32':
        os.system('color 71')
        # os.system(f'mode con cols={_width} lines={_hight}')
    else:
        os.system('setterm -background white -foreground white -store')
        # ubuntu terminal
        os.system('setterm -term linux -back $blue -fore white -clear')
    PATH_SCRIPT = os.path.abspath(os.path.dirname(__file__))
    os.chdir(PATH_SCRIPT)
    clear_console()
    check_version()

    authorship(__author__, __title__, __version__, __copyright__)

    try:
        main()
        time.sleep(3)
        # input('---------------   END   ---------------')
    except KeyboardInterrupt:
        print('Отмена. Скрипт остановлен.')
        exit_from_program(code=0)
    except Exception as e:
        print(e)
        raise e
        exit_from_program(code=1)
